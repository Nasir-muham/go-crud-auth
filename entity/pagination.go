package entity

type PageFetchInput struct {
	Page *int
	Size *int
	Sort string
}
