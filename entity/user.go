package entity

import (
	null "gopkg.in/guregu/null.v3"
)

// User struct
type User struct {
	ID           int64     `json:"id"`
	Username     string    `json:"username"`
	Email        string    `json:"email"`
	Password     string    `json:"password,omitempty"`
	CreatedBy    string    `json:"created_by"`
	CreatedAt    null.Time `json:"created_at"`
	UpdatedAt    null.Time `json:"updated_at"`
	LastUpdateBy *string   `json:"last_update_by"`
	IsDeleted    bool      `json:"is_deleted"`
	Role         string    `json:"role"`
}

// Users list
type Users []User
