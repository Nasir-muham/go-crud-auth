package main

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"
	api "gitlab.com/matrix_2/be_matrix_02/delivery"
	_userPostgres "gitlab.com/matrix_2/be_matrix_02/domain/user/postgres"
	"gitlab.com/matrix_2/be_matrix_02/service"
	conn "gitlab.com/matrix_2/be_matrix_02/util/database"
)

func init() {
	log.SetPrefix("[API-Backend Service] ")
	log.SetFlags(log.LstdFlags | log.Lshortfile)
}
func main() {

	var (
		cfg, err = loadConfig()
	)
	if err != nil {
		log.Println(err)
	}
	db, err := conn.Init(cfg.Database)
	if err != nil {
		panic(err)
	}
	log.Println("Database successfully initialized")

	// User
	userPostgres := _userPostgres.New(db)
	log.Println("User postgres is successfully initialized")

	service := service.New(userPostgres)
	router := mux.NewRouter()
	api.New(service).Register(router)
	log.Println("API successfully initialized")

	log.Println("Webserver succesfully started")
	log.Println("Listening to port ", cfg.Webserver)

	log.Fatal(http.ListenAndServe(cfg.Webserver, router))
}
