package main

import (
	"fmt"
	"log"
	"os"

	"github.com/joho/godotenv"
	conn "gitlab.com/matrix_2/be_matrix_02/util/database"
)

type config struct {
	Database  conn.DBConfig
	Webserver string
}

func loadConfig() (*config, error) {
	var cfg config
	_ = godotenv.Load()
	log.Println("Starting API Backend Service")
	database := conn.DBConfig{
		Host:     os.Getenv("DB_HOST"),
		Port:     os.Getenv("DB_PORT"),
		Name:     os.Getenv("DB_NAME"),
		Username: os.Getenv("DB_USER"),
		Password: os.Getenv("DB_PASSWORD"),
		Dialect:  os.Getenv("DB_DIALECT"),
	}

	webserver := os.Getenv("WEBSERVER_LISTEN_ADDRESS")
	if os.Getenv("PORT") != "" {
		webserver = fmt.Sprintf(":%v", os.Getenv("PORT"))
	}
	cfg = config{
		Database:  database,
		Webserver: webserver,
	}
	return &cfg, nil
}
