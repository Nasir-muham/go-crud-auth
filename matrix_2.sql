/*
PostgreSQL Backup
Database: matrix_2/public
Backup Time: 2021-03-16 12:02:25
*/

DROP SEQUENCE IF EXISTS "public"."category_id_seq";
DROP SEQUENCE IF EXISTS "public"."customer_id_seq";
DROP SEQUENCE IF EXISTS "public"."employee_id_seq";
DROP SEQUENCE IF EXISTS "public"."menu_id_seq";
DROP SEQUENCE IF EXISTS "public"."notes_id_seq";
DROP SEQUENCE IF EXISTS "public"."order_detail_id_seq";
DROP SEQUENCE IF EXISTS "public"."orders_id_seq";
DROP SEQUENCE IF EXISTS "public"."position_id_seq";
DROP SEQUENCE IF EXISTS "public"."users_id_seq";
DROP TABLE IF EXISTS "public"."users";
CREATE SEQUENCE "category_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE SEQUENCE "customer_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE SEQUENCE "employee_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE SEQUENCE "menu_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE SEQUENCE "notes_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;
CREATE SEQUENCE "order_detail_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE SEQUENCE "orders_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE SEQUENCE "position_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE SEQUENCE "users_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE TABLE "users" (
  "id" int4 NOT NULL DEFAULT nextval('users_id_seq'::regclass),
  "username" varchar(45) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "email" varchar(45) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "password" varchar(100) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "created_at" timestamp(0) DEFAULT NULL::timestamp without time zone,
  "created_by" varchar(45) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "updated_at" timestamp(0) DEFAULT NULL::timestamp without time zone,
  "last_update_by" varchar(100) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "is_deleted" bool,
  "role" varchar(255) COLLATE "pg_catalog"."default"
)
;
ALTER TABLE "users" OWNER TO "postgres";
BEGIN;
LOCK TABLE "public"."users" IN SHARE MODE;
DELETE FROM "public"."users";
INSERT INTO "public"."users" ("id","username","email","password","created_at","created_by","updated_at","last_update_by","is_deleted","role") VALUES (1, 'nazyli', 'evrynazyli@gmail.com', '$2a$10$BGMEQtjvlQB9/8lQXryjvez1Xug.XuZJ3P1ajVCiNFbh4qYIL91my', '2020-01-01 02:02:04', 'eb55808b848359c7566d41a69d712cc7d421dca3', '2020-06-12 06:55:37', 'eb55808b848359c7566d41a69d712cc7d421dca3', 'f', 'SPV'),(2, 'evry', 'n4zyl1evry98@gmail.com', '$2a$10$t808d2xGb30KrZUa3STtL.oqxoGmacQA1QDGSCz1nuDzELDOJnctO', '2020-06-10 04:53:47', 'eb55808b848359c7566d41a69d712cc7d421dca3', NULL, NULL, 'f', 'HRD'),(3, 'evry1', 'evry1@gmail.com', '$2a$10$JgOduVWNSufbo.pwEjV25OBFZF7HDsSotSdRlqLyT.Vc6Ta1Ju9aq', '2020-06-10 04:56:10', '6aa2e457db764c796aa79185562ade45e1bbead5', NULL, NULL, 'f', 'Talent'),(7, 'evry', '221@gmail.com', '$2a$10$dbNaT4zFQ6vPKsbrLk1QduCoQvQk4SszLeC.oGZFwxzE5wIx8Cky2', '2021-03-16 11:18:37', '1', '2021-03-16 12:01:47', '2', 't', 'HRD');
COMMIT;
ALTER TABLE "users" ADD CONSTRAINT "users_pkey" PRIMARY KEY ("id");
SELECT setval('"category_id_seq"', 2, false);
ALTER SEQUENCE "category_id_seq" OWNER TO "postgres";
SELECT setval('"customer_id_seq"', 2, false);
ALTER SEQUENCE "customer_id_seq" OWNER TO "postgres";
SELECT setval('"employee_id_seq"', 2, false);
ALTER SEQUENCE "employee_id_seq" OWNER TO "postgres";
SELECT setval('"menu_id_seq"', 2, false);
ALTER SEQUENCE "menu_id_seq" OWNER TO "postgres";
SELECT setval('"notes_id_seq"', 2, false);
ALTER SEQUENCE "notes_id_seq" OWNER TO "postgres";
SELECT setval('"order_detail_id_seq"', 2, false);
ALTER SEQUENCE "order_detail_id_seq" OWNER TO "postgres";
SELECT setval('"orders_id_seq"', 2, false);
ALTER SEQUENCE "orders_id_seq" OWNER TO "postgres";
SELECT setval('"position_id_seq"', 2, false);
ALTER SEQUENCE "position_id_seq" OWNER TO "postgres";
SELECT setval('"users_id_seq"', 9, true);
ALTER SEQUENCE "users_id_seq" OWNER TO "postgres";
