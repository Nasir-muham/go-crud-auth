package middlewares

import (
	"net/http"

	"gitlab.com/matrix_2/be_matrix_02/util/auth"
	"gitlab.com/matrix_2/be_matrix_02/util/responses"
)

// SetMiddlewareJSON . . .
func SetMiddlewareJSON(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json; charset=utf-8")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Credentials", "true")
		w.Header().Set("Access-Control-Expose-Headers", "Authorization")
		w.Header().Add("Access-Control-Expose-Headers", "responseType")
		w.Header().Add("Access-Control-Expose-Headers", "observe")
		w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
		w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
		next(w, r)
	}
}

// SetMiddlewareAuthentication . . .
func SetMiddlewareAuthentication(next http.HandlerFunc, scopes ...string) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		header := w.Header()
		header.Add("Access-Control-Allow-Origin", "*")
		header.Add("Access-Control-Allow-Methods", "DELETE, POST, GET, PATCH,OPTIONS")
		header.Add("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With")
		errMsg := auth.TokenValid(r, scopes...)
		if errMsg != "" {
			responses.ERROR(w, http.StatusUnauthorized, "Unauthorized, "+errMsg)
			return
		}
		next(w, r)
	}
}
