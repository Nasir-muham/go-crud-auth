package dbdialect

import (
	"strconv"

	"github.com/jmoiron/sqlx"
	"gitlab.com/matrix_2/be_matrix_02/entity"
)

// SeQuery
type Postgres struct {
	db *sqlx.DB
}

// New init Postgres
func New(db *sqlx.DB) *Postgres {
	return &Postgres{db}
}

func (m *Postgres) SetQuery(query string, p *entity.PageFetchInput) (newQuery string) {

	if p != nil {
		if p.Sort != "" {
			query += " ORDER BY " + p.Sort
		}
		if p.Size != nil {
			sz := *p.Size
			size := strconv.Itoa(sz)
			query += " LIMIT " + size
			if p.Page != nil {
				pg := *p.Page
				page := strconv.Itoa(pg * sz)
				query += " OFFSET " + page
			}
		}
	}
	return query
}
