package service

import (
	"context"
	"crypto/sha1"
	"database/sql"
	"fmt"
	"log"
	"net/http"
	"time"

	"gitlab.com/matrix_2/be_matrix_02/entity"
	"gitlab.com/matrix_2/be_matrix_02/util/auth"
	"golang.org/x/crypto/bcrypt"
	null "gopkg.in/guregu/null.v3"
)

func (s *svc) SignIn(ctx context.Context, email, password string) (token *auth.Token, status Status) {
	user, err := s.user.GetByEmail(ctx, email)
	if err != nil {
		log.Println(err)
		if err == sql.ErrNoRows {
			return nil, Status{http.StatusNotFound, "Email"}
		}
		return nil, Status{http.StatusInternalServerError, "Email"}
	}

	err = VerifyPassword(user.Password, password)
	if err != nil && err == bcrypt.ErrMismatchedHashAndPassword {
		log.Println(err)
		return nil, Status{http.StatusUnprocessableEntity, "Incorrect Password"}

	}
	token, err = auth.CreateToken(user.Username, &user.Role)
	if err != nil {
		log.Println(err)
		return nil, Status{http.StatusUnprocessableEntity, "Generate Token"}
	}
	return token, Status{http.StatusOK, ""}
}
func (s *svc) GetUserByID(ctx context.Context, id int64) (user *entity.User, status Status) {
	user, err := s.user.GetByID(ctx, id)
	if err == sql.ErrNoRows {
		log.Println(err)
		return user, Status{http.StatusNotFound, "User"}
	}
	if err != nil {
		log.Println(err)
		return user, Status{http.StatusInternalServerError, "User"}
	}
	return user, Status{http.StatusOK, ""}
}

func (s *svc) SelectUsers(ctx context.Context, search string, p *entity.PageFetchInput) (users entity.Users, totalUser int, status Status) {
	user, total, err := s.user.Select(ctx, search, p)
	if err != nil {
		log.Println(err)
		return user, total, Status{http.StatusInternalServerError, ""}
	}
	return user, total, Status{http.StatusOK, ""}
}
func (s *svc) InsertUser(ctx context.Context, user *entity.User) (userData *entity.User, status Status) {
	_, err := s.user.GetByEmail(ctx, user.Email)
	if err == nil {
		return nil, Status{http.StatusFound, "Email has been used"}
	}
	hashedPassword, err := Hash(user.Password)
	if err != nil {
		log.Println(err)
		return nil, Status{http.StatusInternalServerError, "User"}
	}
	password := string(hashedPassword)

	// Add
	user.Password = password
	user.CreatedAt = null.TimeFrom(time.Now())
	user.IsDeleted = false
	err = s.user.Insert(ctx, user)
	if err != nil {
		log.Println(err)
		return nil, Status{http.StatusInternalServerError, "User"}
	}

	return user, Status{http.StatusOK, ""}
}
func (s *svc) UpdateUser(ctx context.Context, id int64, user *entity.User) (userData *entity.User, status Status) {
	var (
		err error
	)
	getUser, status := s.GetUserByID(ctx, id)
	if status.Code != http.StatusOK {
		return user, status
	}
	if getUser.Email != user.Email {
		_, err = s.user.GetByEmail(ctx, user.Email)
		if err == nil {
			return nil, Status{http.StatusFound, "Email has been used"}
		}
	}
	hashedPassword, err := Hash(user.Password)
	if err != nil {
		log.Println(err)
		return nil, Status{http.StatusInternalServerError, "User"}
	}
	if user.Password != "" {
		user.Password = string(hashedPassword)
	} else {
		user.Password = getUser.Password
	}

	user.UpdatedAt = null.TimeFrom(time.Now())
	user.ID = id
	user.CreatedBy = getUser.CreatedBy
	err = s.user.Update(ctx, user)
	if err != nil {
		log.Println(err)
		return user, Status{http.StatusInternalServerError, "User"}
	}

	// kirim response
	userData, status = s.GetUserByID(ctx, id)
	if status.Code != http.StatusOK {
		return user, status
	}
	return userData, Status{http.StatusOK, ""}
}

func (s *svc) DeleteUser(ctx context.Context, id int64, userid string) (status Status) {
	_, status = s.GetUserByID(ctx, id)
	if status.Code != http.StatusOK {
		return status
	}
	user := &entity.User{
		ID:           id,
		UpdatedAt:    null.TimeFrom(time.Now()),
		LastUpdateBy: &userid,
	}
	err := s.user.Delete(ctx, user)
	if err != nil {
		log.Println(err)
		return Status{http.StatusInternalServerError, "User"}
	}
	return Status{http.StatusOK, ""}
}

// VerifyPassword . . .
func VerifyPassword(hashedPassword, password string) error {
	return bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(password))
}

// HashSHA1 . .
func HashSHA1(text string) (hash string) {
	var sha = sha1.New()
	sha.Write([]byte(text))
	encrypted := sha.Sum(nil)
	hash = fmt.Sprintf("%x", encrypted)
	return
}

// Hash . . .
func Hash(password string) ([]byte, error) {
	return bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
}
