package service

import (
	"context"

	_user "gitlab.com/matrix_2/be_matrix_02/domain/user"
	"gitlab.com/matrix_2/be_matrix_02/entity"
	"gitlab.com/matrix_2/be_matrix_02/util/auth"
)

// Status . . .
type Status struct {
	Code   int
	ErrMsg string
}
type svc struct {
	user _user.Repository
}

// New init service
func New(user _user.Repository) Service {
	return &svc{
		user: user,
	}
}

// Service . .
type Service interface {
	SignIn(ctx context.Context, email, password string) (token *auth.Token, status Status)
	// User
	GetUserByID(ctx context.Context, id int64) (user *entity.User, status Status)
	SelectUsers(ctx context.Context, search string, p *entity.PageFetchInput) (users entity.Users, totalUser int, status Status)
	InsertUser(ctx context.Context, user *entity.User) (userData *entity.User, status Status)
	UpdateUser(ctx context.Context, id int64, user *entity.User) (userData *entity.User, status Status)
	DeleteUser(ctx context.Context, id int64, userid string) (status Status)
}
