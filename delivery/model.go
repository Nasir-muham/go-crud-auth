package delivery

//DataResponse json
type DataResponse struct {
	Total      interface{} `json:"totalItems,omitempty"`
	Type       string      `json:"type,omitempty"`
	Attributes interface{} `json:"attributes,omitempty"`
	Message    interface{} `json:"message,omitempty"`
}
type reqLogin struct {
	Email    string `json:"email" validate:"required,email"`
	Password string `json:"password" validate:"required"`
}
type reqUser struct {
	Username string `json:"username" validate:"required"`
	Email    string `json:"email" validate:"required,email"`
	Password string `json:"password"`
	Role     string `json:"role" validate:"required"`
	UserId   string `json:"userid" validate:"required"`
}
type reqDelete struct {
	UserId string `json:"userid" validate:"required"`
}
