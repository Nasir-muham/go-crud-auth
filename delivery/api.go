package delivery

import (
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"gitlab.com/matrix_2/be_matrix_02/entity"
	_service "gitlab.com/matrix_2/be_matrix_02/service"
	"gitlab.com/matrix_2/be_matrix_02/util/middlewares"
)

// API struct
type API struct {
	Service _service.Service
}

func New(service _service.Service) *API {
	return &API{
		Service: service,
	}
}
func (api *API) Register(r *mux.Router) {
	r.HandleFunc("/", api.HandleGetHello).Methods("GET")
	r.HandleFunc("/ping", middlewares.SetMiddlewareJSON(api.HandleGetPing)).Methods("GET")
	r.HandleFunc("/login", middlewares.SetMiddlewareJSON(api.Login)).Methods("POST", "OPTIONS")
	// User
	r.HandleFunc("/user/{id}", middlewares.SetMiddlewareAuthentication(api.HandleGetUserById)).Methods("GET")
	r.HandleFunc("/user", middlewares.SetMiddlewareAuthentication(api.HandleSelectUsers)).Methods("GET")
	r.HandleFunc("/user", middlewares.SetMiddlewareAuthentication(api.HandlePostUsers)).Methods("POST")
	r.HandleFunc("/user/{id}", middlewares.SetMiddlewareAuthentication(api.HandlePatchUsers)).Methods("PATCH")
	r.HandleFunc("/user/{id}", middlewares.SetMiddlewareAuthentication(api.HandleDeleteUsers)).Methods("DELETE")
}

func (api *API) HandleGetPing(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("PONG"))
}
func (api *API) HandleGetHello(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Welcome to api backend service"))
}
func (api *API) getPagination(w http.ResponseWriter, r *http.Request) (p *entity.PageFetchInput, e string) {
	var (
		getParam = r.URL.Query()
		page     *int
		size     *int
	)
	allParams := getParam.Get("size")
	if allParams != "" {
		sz, err := strconv.Atoi(allParams)
		if err != nil {
			return p, "size is not an integer"
		}
		size = &sz
		allParams := getParam.Get("page")
		if allParams != "" {
			pg, err := strconv.Atoi(allParams)
			if err != nil {
				return p, "page is not an integer"
			}
			page = &pg
		}
	}

	p = &entity.PageFetchInput{
		Page: page,
		Size: size,
		Sort: getParam.Get("sort"),
	}
	return p, ""
}
