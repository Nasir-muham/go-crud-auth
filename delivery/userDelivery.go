package delivery

import (
	"encoding/json"
	"log"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"gitlab.com/matrix_2/be_matrix_02/entity"
	"gitlab.com/matrix_2/be_matrix_02/util/responses"
	"gopkg.in/go-playground/validator.v9"
)

func (api *API) Login(w http.ResponseWriter, r *http.Request) {
	var (
		params reqLogin
	)
	err := json.NewDecoder(r.Body).Decode(&params)
	if err != nil {
		log.Println(err)
		responses.ERROR(w, http.StatusBadRequest, "Invalid Parameter")
		return
	}
	v := validator.New()
	err = v.Struct(params)
	if err != nil {
		log.Println(err)
		responses.ERROR(w, http.StatusBadRequest, "Invalid Parameter")
		return
	}
	token, status := api.Service.SignIn(r.Context(), params.Email, params.Password)
	if status.Code != http.StatusOK {
		responses.ERROR(w, status.Code, "Failed", status.ErrMsg)
		return
	}
	responses.OK(w, token)
}

func (api *API) HandleGetUserById(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	id, err := strconv.ParseInt(params["id"], 10, 64)
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, "ID must Integer")
		return
	}

	user, status := api.Service.GetUserByID(r.Context(), id)
	if status.Code != http.StatusOK {
		responses.ERROR(w, status.Code, "Failed Get User", status.ErrMsg)
		return
	}

	res := DataResponse{
		Type: "User",
		Attributes: entity.User{
			ID:           user.ID,
			Username:     user.Username,
			Email:        user.Email,
			CreatedAt:    user.CreatedAt,
			CreatedBy:    user.CreatedBy,
			UpdatedAt:    user.UpdatedAt,
			LastUpdateBy: user.LastUpdateBy,
			Role:         user.Role,
			IsDeleted:    user.IsDeleted,
		},
	}
	responses.OK(w, res)

}

func (api *API) HandleSelectUsers(w http.ResponseWriter, r *http.Request) {
	var (
		getParam = r.URL.Query()
	)
	pagination, e := api.getPagination(w, r)
	if e != "" {
		responses.ERROR(w, http.StatusBadRequest, e)
		return
	}
	search := getParam.Get("search")

	user, total, status := api.Service.SelectUsers(r.Context(), search, pagination)
	if status.Code != http.StatusOK {
		responses.ERROR(w, status.Code, "Failed Get Users")
		return
	}

	// display array scope
	res := make([]entity.User, 0, len(user))
	for _, i := range user {
		res = append(res, entity.User{
			ID:           i.ID,
			Username:     i.Username,
			Email:        i.Email,
			CreatedAt:    i.CreatedAt,
			CreatedBy:    i.CreatedBy,
			UpdatedAt:    i.UpdatedAt,
			LastUpdateBy: i.LastUpdateBy,
			IsDeleted:    i.IsDeleted,
			Role:         i.Role,
		})
	}
	respon := DataResponse{
		Total:      total,
		Type:       "User",
		Attributes: res,
	}
	responses.OK(w, respon)

}

func (api *API) HandlePostUsers(w http.ResponseWriter, r *http.Request) {
	var (
		params reqUser
	)
	err := json.NewDecoder(r.Body).Decode(&params)
	if err != nil {
		log.Println(err)
		responses.ERROR(w, http.StatusBadRequest, "Invalid Parameter")
		return
	}
	v := validator.New()
	err = v.Struct(params)
	if err != nil {
		log.Println(err)
		responses.ERROR(w, http.StatusBadRequest, "Invalid Parameter")
		return
	}
	if params.Password == "" {
		responses.ERROR(w, http.StatusBadRequest, "Password cannot be null")
		return
	}
	user := &entity.User{
		Username:  params.Username,
		Email:     params.Email,
		Password:  params.Password,
		Role:      params.Role,
		CreatedBy: params.UserId,
	}
	user, status := api.Service.InsertUser(r.Context(), user)
	if status.Code != http.StatusOK {
		responses.ERROR(w, status.Code, "Failed Insert User", status.ErrMsg)
		return
	}
	res := DataResponse{
		Type: "User",
		Attributes: entity.User{
			ID:           user.ID,
			Username:     user.Username,
			Email:        user.Email,
			CreatedAt:    user.CreatedAt,
			CreatedBy:    user.CreatedBy,
			UpdatedAt:    user.UpdatedAt,
			LastUpdateBy: user.LastUpdateBy,
			Role:         user.Role,
			IsDeleted:    user.IsDeleted,
		},
	}
	responses.OK(w, res)

}

func (api *API) HandlePatchUsers(w http.ResponseWriter, r *http.Request) {
	var (
		params reqUser
	)
	paramsID := mux.Vars(r)
	id, err := strconv.ParseInt(paramsID["id"], 10, 64)
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, "ID must Integer")
		return
	}
	err = json.NewDecoder(r.Body).Decode(&params)
	if err != nil {
		log.Println(err)
		responses.ERROR(w, http.StatusBadRequest, "Invalid Parameter")
		return
	}
	v := validator.New()
	err = v.Struct(params)
	if err != nil {
		log.Println(err)
		responses.ERROR(w, http.StatusBadRequest, "Invalid Parameter")
		return
	}

	user := &entity.User{
		Username:     params.Username,
		Email:        params.Email,
		Role:         params.Role,
		Password:     params.Password,
		LastUpdateBy: &params.UserId,
	}
	user, status := api.Service.UpdateUser(r.Context(), id, user)
	if status.Code != http.StatusOK {
		responses.ERROR(w, status.Code, "Failed Update User", status.ErrMsg)
		return
	}

	res := DataResponse{
		Type: "User",
		Attributes: entity.User{
			ID:           user.ID,
			Username:     user.Username,
			Email:        user.Email,
			CreatedAt:    user.CreatedAt,
			CreatedBy:    user.CreatedBy,
			UpdatedAt:    user.UpdatedAt,
			LastUpdateBy: user.LastUpdateBy,
			IsDeleted:    user.IsDeleted,
			Role:         user.Role,
		},
	}
	responses.OK(w, res)

}

func (api *API) HandleDeleteUsers(w http.ResponseWriter, r *http.Request) {
	var (
		params reqDelete
	)
	paramsID := mux.Vars(r)
	id, err := strconv.ParseInt(paramsID["id"], 10, 64)
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, "ID must Integer")
		return
	}
	err = json.NewDecoder(r.Body).Decode(&params)
	if err != nil {
		log.Println(err)
		responses.ERROR(w, http.StatusBadRequest, "Invalid Parameter")
		return
	}
	v := validator.New()
	err = v.Struct(params)
	if err != nil {
		log.Println(err)
		responses.ERROR(w, http.StatusBadRequest, "Invalid Parameter")
		return
	}

	status := api.Service.DeleteUser(r.Context(), id, params.UserId)
	if status.Code != http.StatusOK {
		responses.ERROR(w, status.Code, "Failed Delete User", status.ErrMsg)
		return
	}
	res := DataResponse{
		Type:    "User",
		Message: "Data Berhasil Di Hapus",
	}
	responses.OK(w, res)
}
