package user

import (
	"context"

	"gitlab.com/matrix_2/be_matrix_02/entity"
)

// Repository User inteface
type Repository interface {
	Select(ctx context.Context, search string, p *entity.PageFetchInput) (users entity.Users, totalUser int, err error)
	GetByEmail(ctx context.Context, email string) (user *entity.User, err error)
	GetByID(ctx context.Context, id int64) (user *entity.User, err error)
	Insert(ctx context.Context, user *entity.User) (err error)
	Update(ctx context.Context, user *entity.User) (err error)
	Delete(ctx context.Context, user *entity.User) (err error)
}
