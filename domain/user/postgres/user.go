package postgres

import (
	"context"
	"database/sql"

	"github.com/jmoiron/sqlx"
	"gitlab.com/matrix_2/be_matrix_02/entity"
	"gitlab.com/matrix_2/be_matrix_02/util/dbdialect"
)

// PostgresSQL struct
type PostgresSQL struct {
	db *sqlx.DB
}

// New init PostgresSQL
func New(db *sqlx.DB) *PostgresSQL {
	return &PostgresSQL{db}
}

// GetByEmail . . .
func (m *PostgresSQL) GetByEmail(ctx context.Context, email string) (user *entity.User, err error) {
	var u User
	query := `
	SELECT
		id,
		username,
		email,
		password,
		role 
	FROM
		users
	WHERE is_deleted = false AND email = $1
		`
	err = m.db.GetContext(ctx, &u, query, email)
	if err != nil {
		return nil, err
	}
	user = &entity.User{
		ID:       u.ID,
		Username: u.Username,
		Email:    u.Email,
		Password: u.Password,
		Role:     u.Role,
	}
	return user, nil
}

// GetByID . . .
func (m *PostgresSQL) GetByID(ctx context.Context, id int64) (user *entity.User, err error) {
	var (
		u    User
		args []interface{}
	)
	query := `
	SELECT
		id,
		username,
		email,
		password,
		created_at,
		created_by,
		updated_at,
		last_update_by,
		is_deleted,
		role
	FROM
		users
	WHERE
		id = $1 AND is_deleted = false
		`
	args = append(args, id)
	err = m.db.GetContext(ctx, &u, query, args...)
	if err != nil {
		return nil, err
	}
	user = &entity.User{
		ID:           u.ID,
		Username:     u.Username,
		Email:        u.Email,
		CreatedAt:    u.CreatedAt,
		CreatedBy:    u.CreatedBy,
		UpdatedAt:    u.UpdatedAt,
		LastUpdateBy: u.LastUpdateBy,
		Role:         u.Role,
		IsDeleted:    u.IsDeleted,
		Password:     u.Password,
	}
	return user, nil
}

// Select . . .
func (m *PostgresSQL) Select(ctx context.Context, search string, p *entity.PageFetchInput) (users entity.Users, totalUser int, err error) {
	var (
		u     Users
		total Users
		args  []interface{}
	)
	query := `
	SELECT
		id,
		username,
		email,
		created_at,
		created_by,
		updated_at,
		last_update_by,
		is_deleted,
		role
	FROM
		users WHERE is_deleted = false AND username ILIKE '%' || $1 || '%'
		`
	args = append(args, search)
	q := dbdialect.New(m.db).SetQuery(query, p)
	err = m.db.SelectContext(ctx, &u, q, args...)
	if err != nil {
		return nil, 0, err
	}
	for _, i := range u {
		users = append(users, entity.User{
			ID:           i.ID,
			Username:     i.Username,
			Email:        i.Email,
			CreatedAt:    i.CreatedAt,
			CreatedBy:    i.CreatedBy,
			UpdatedAt:    i.UpdatedAt,
			LastUpdateBy: i.LastUpdateBy,
			IsDeleted:    i.IsDeleted,
			Role:         i.Role,
		})
	}
	_ = dbdialect.New(m.db).SetQuery(query, nil)
	_ = m.db.SelectContext(ctx, &total, query, args...)
	return users, len(total), nil
}

func (m *PostgresSQL) Insert(ctx context.Context, user *entity.User) (err error) {
	query := `
	INSERT INTO users
		(
			username,
			email,
			password,
			created_at,
			created_by,
			last_update_by,
			updated_at,
			is_deleted,
			role
		) 
		VALUES 
		(
			:username,
			:email,
			:password,
			:created_at,
			:created_by,
			:last_update_by,
			:updated_at,
			:is_deleted,
			:role
		) RETURNING id ;
	`
	tx, err := m.db.Beginx()
	if err != nil {
		return err
	}
	defer tx.Rollback()
	res, err := m.db.NamedQueryContext(ctx, query, &User{
		Username:     user.Username,
		Email:        user.Email,
		Password:     user.Password,
		CreatedAt:    user.CreatedAt,
		CreatedBy:    user.CreatedBy,
		UpdatedAt:    user.CreatedAt,
		LastUpdateBy: &user.CreatedBy,
		IsDeleted:    user.IsDeleted,
		Role:         user.Role,
	})
	if err != nil {
		return err
	}
	if res.Next() {
		res.Scan(&user.ID)
	}
	if err != nil {
		return err
	}
	err = tx.Commit()
	if err != nil {
		return err
	}
	return err
}
func (m *PostgresSQL) Update(ctx context.Context, user *entity.User) (err error) {
	query := `
	UPDATE 
		users
	SET
		username =:username,
		email =:email,
		updated_at =:updated_at,
		last_update_by =:last_update_by,
		role = :role,
		password=:password
	WHERE 
		id = :id AND
		is_deleted = false
	`
	tx, err := m.db.Beginx()
	if err != nil {
		return err
	}
	defer tx.Rollback()
	res, err := m.db.NamedExecContext(ctx, query, &User{
		ID:           user.ID,
		Username:     user.Username,
		Email:        user.Email,
		CreatedBy:    user.CreatedBy,
		UpdatedAt:    user.UpdatedAt,
		LastUpdateBy: user.LastUpdateBy,
		IsDeleted:    user.IsDeleted,
		Role:         user.Role,
		Password:     user.Password,
	})
	if err != nil {
		return err
	}
	num, err := res.RowsAffected()
	if err != nil {
		return err
	}
	if num == 0 {
		return sql.ErrNoRows
	}
	err = tx.Commit()
	if err != nil {
		return err
	}
	return err
}

func (m *PostgresSQL) Delete(ctx context.Context, user *entity.User) (err error) {
	query := `
	UPDATE
		users
	SET
		is_deleted = true,
		last_update_by = :last_update_by,
		updated_at = :updated_at
	WHERE
		id = :id AND
		is_deleted = false `

	res, err := m.db.NamedExecContext(ctx, query, &User{
		ID:           user.ID,
		UpdatedAt:    user.UpdatedAt,
		LastUpdateBy: user.LastUpdateBy,
		CreatedBy:    user.CreatedBy,
	})
	if err != nil {
		return err
	}
	num, err := res.RowsAffected()
	if err != nil {
		return err
	}
	if num == 0 {
		return sql.ErrNoRows
	}
	return err
}
