package postgres

import (
	"gopkg.in/guregu/null.v3"
)

// User struct
type User struct {
	ID           int64     `db:"id"`
	Username     string    `db:"username"`
	Email        string    `db:"email"`
	Password     string    `db:"password"`
	CreatedAt    null.Time `db:"created_at"`
	CreatedBy    string    `db:"created_by"`
	UpdatedAt    null.Time `db:"updated_at"`
	LastUpdateBy *string   `db:"last_update_by"`
	IsDeleted    bool      `db:"is_deleted"`
	Role         string    `db:"role"`
}

// Users list
type Users []User
